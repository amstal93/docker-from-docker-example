# Faking Docker In Docker

## Introduction

You are running a Docker container and you need to run another container.

Run a sibling container. This is also called Docker From Docker

> With this approach, a container, with Docker installed, does not run its own Docker daemon, but connects to the Docker daemon of the host system. That means, you will have a Docker CLI in the container, as well as on the host system, but they both connect to one and the same Docker daemon. At any time, there is only one Docker daemon running in your machine, the one running on the host system.
>
> To achieve this, you can start a Docker container, that has Docker installed, with the following bind mount option:
>
>   `-v /var/run/docker.sock:/var/run/docker.sock`
> 
> -- <cite>[Docker in Docker?](https://itnext.io/docker-in-docker-521958d34efd)</cite>

There is a gotcha. You must create a docker group inside the container with the same GID as the docker group on your Docker host. The user inside the container must be a member of the docker group.

See [Docker in Docker permissions error](https://stackoverflow.com/questions/42164653/docker-in-docker-permissions-error)

I use my containers with bind mounts for code etc.
It is important to run the container user with the same uid and gid as the host so that files are written with the correct ownership.
The user also needs to be a member of the docker group with the correct gid, as previously mentioned.

These scripts and the Dockerfile show how to do this. Changes will be required for different projects.

## Using Docker Compose

Note: In addition the build script/Dockefile adds the current version of Docker Compose V2. You can test this as follows

1. Run the "outer" container via `./run.sh`

2. From the new container prompt type Docker compose commands. For example

   a. `docker compose -f docker-compose/docker/dev-compose.yaml build`

   b. `docker compose -f docker-compose/docker/dev-compose.yaml run python`

NOTE: When setting up bind mounts the source path must be relative to the host filesystem, not the "outer" container filesystem.
This why the example script passes in an environment variable with the workspace root.
More info [here](https://code.visualstudio.com/remote/advancedcontainers/use-docker-kubernetes#_mounting-host-volumes-with-docker-from-inside-a-container)

## See also:

* [vscode-dev-containers/containers/docker-from-docker at main · microsoft/vscode-dev-containers](https://github.com/microsoft/vscode-dev-containers/tree/main/containers/docker-from-docker)

* [Use Docker or Kubernetes from a container](https://code.visualstudio.com/remote/advancedcontainers/use-docker-kubernetes)
